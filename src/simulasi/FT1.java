package simulasi;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class FT1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

//		NO 01 

//		System.out.print("Input Bilangan : ");
//		int n = input.nextInt();
//		int bilangan;
//		
//		for (int i = 0; i <= n; i++) { //bilangan ganjil
//			if (i % 2 == 1) {
//				System.out.print(i + " ");
//			}
//		}
//		System.out.println();
//		
//		for (int j = 1; j <= n; j++) { //bilangan genap
//			if (j % 2 == 0) {
//				System.out.print(j + " ");
//			}
//		}

//		Cara KEDUA NO 01
		System.out.print("Input Angka = ");
		int n = input.nextInt();

		String ganjil = "";
		String genap = "";

		for (int i = 1; i <= n; i++) {
			if (i % 2 == 1) {
				ganjil += (i + " ");
			} else if (i % 2 == 0) {
				genap += (i + " ");
			}
		}

		System.out.println("Angka Ganjil" + ganjil);
		System.out.println("Angka Genap" + genap);

//		NO 02

//		System.out.println("Silahkan ketik : ");
//	
//		String kata =input.nextLine().toLowerCase().replaceAll(" ", "");
//		
//		char[]huruf =kata.toCharArray();
//		String vokal = "";
//		String konsonan ="";
//		
//		
//		Arrays.sort(huruf);
//		for (int i = 0; i < huruf.length; i++) {
//			if(huruf[i] == 'a' || huruf[i] == 'i' || huruf[i] == 'u'
//					|| huruf[i]=='e' || huruf[i] == 'o') {
//				vokal += huruf[i];
//			}else {
//				konsonan += huruf[i];
//			}
//		}
//		System.out.println("Huruf Vokal adalah : " + vokal);
//		System.out.println("Huruf Konsonan adalah : " + konsonan);

//		NO 03

//		System.out.println("Input nilai : ");
//		int nilai = input.nextInt();
//		
//		int nilaiAwal=99;
//		int nilaiTambah =1;
//		
//		for (int i = 0; i < nilai; i++) {
//			nilaiAwal =nilaiAwal+nilaiTambah;
//			
//			System.out.println(nilaiAwal+ " adalah " + "Si Angka 1");
//			nilaiTambah=nilaiTambah*3;
//			nilaiAwal=100;
//			
//		}

		// cara KEDUA NO 03
//		System.out.println("Input n : ");
//		int bilangan = input.nextInt();
//
//		int siAngka1 = 100;
//		for (int i = 0; i < bilangan; i++) {
//			int pangkat = (int) Math.pow(3, i);
//			siAngka1 = 100 + pangkat;
//			if (i == 0) {
//				siAngka1 = 100;
//			}
//			System.out.println(siAngka1 + " " + "adalah Si Angka 1  ");
//		}

//			NO 04

//			System.out.println("Masukkan Jarak: ");
//			int tempJarak[]= new int[4]; //membuat variabel int array ,4 input yg kita inginkan, 
//			double total =0;
//			double bensin =0;
//			for (int i = 0; i < 4; i++) { //4 input yg kita inginkan
//				int jarak = input.nextInt();
//				tempJarak[i] = jarak ;
//			}
//			input.nextLine();
//			System.out.println("Masukkan Indeks Jarak :");
//			String[] inputIndeksSplit = input.nextLine().split(" "); //membuat variabel split String indeks ke 0 1 2 3 menjadi 0123
//			
//			for (int i = 0; i < inputIndeksSplit.length; i++) {
//				total += tempJarak[Integer.parseInt(inputIndeksSplit[i])-1]; //parseint untuk mencetak integer array . -1 untuk memulai dari indeks ke 0(1-1)
//			}
//			System.out.println(total);
//			bensin = total / 2500;
//			System.out.println(Math.ceil(bensin)); //mathceil membulatkan nilai keatas

//		CARA KEDUA NO 04
//		int tempJarak[] = new int[4];

//		 Masukan Jarak
//		tempJarak[0] = 2000;
//		tempJarak[1] = 500;
//		tempJarak[2] = 1500;
//		tempJarak[3] = 2500;
// 
//		int tempJarak[] = {2000,500,1500,2500};
// 
//		 Memasukan Inputan
//		System.out.println("Rute : ");
//		System.out.println("Toko = 1 ");
//		System.out.println("Tempat 1 = 2");
//		System.out.println("Tempat 2 = 3");
//		System.out.println("Tempat 3 = 4");
// 
//		System.out.println("Masukan Rute : ");
//		String rute[] = input.nextLine().split(" ");
// 
//		Menghitung total dan bensin
//		double total = 0;
//		double bensin = 0;
// 
//		for( int i = 0; i < rute.length; i++) {
//			total+= tempJarak[Integer.parseInt(rute[i]) - 1 ];
//		}
// 
//		System.out.println();
// 
//		System.out.println(total);
// 
//		bensin = total / 3000;
//		int totalBensin = (int) Math.ceil(bensin);
//		System.out.println("Total bensin : " + totalBensin + " Liter");

//		NO 05

//		System.out.println("masukkan jumlah laki-laki dewasa : ");
//		int lakiDewasa = input.nextInt();
//		System.out.println("masukkan jumlah perempuan dewasa : ");
//		int perempuanDewasa = input.nextInt();
//		System.out.println("masukkan jumlah remaja : ");
//		int remaja = input.nextInt();
//		System.out.println("masukkan jumlah anak : ");
//		int anak = input.nextInt();
//		System.out.println("masukkan jumlah balita : ");
//		int balita = input.nextInt();
// 
//		double nasgor = 0;
//		int mieAyam = 0;
//		int mie = 0;
//		int bubur = 0;
//		int totalOrang = lakiDewasa + perempuanDewasa + remaja + balita;
// 
//		if (totalOrang % 2 != 0 && totalOrang > 5) {
//			perempuanDewasa = perempuanDewasa + 1;
//		}
// 
//		nasgor = nasgor + 2 * lakiDewasa + (double)(anak)/2;
//		mieAyam = mieAyam + remaja;
//		mie = mie + perempuanDewasa;
//		bubur = bubur + balita;
//		int jumlah = (int) Math.ceil(nasgor) + mieAyam + mie + bubur;
//		System.out.println("jumlah porsi nasgor = " + nasgor);
//		System.out.println("jumlah porsi mieAyam = " + mieAyam);
//		System.out.println("jumlah porsi mie = " + mie);
//		System.out.println("jumlah porsi bubur = " + bubur);
// 
//		System.out.println("jumlah porsi keseluruhan = " + jumlah);

//		CARA KEDUA NO 05
//		System.out.println("Masukan jumlah laki-laki Dewasa : ");
//		int lakiDewasa = input.nextInt();
//		
//		System.out.println("Masukan jumlah Perempuan Dewasa : ");
//		int perempuanDewasa = input.nextInt();
//		
//		System.out.println("Masukan jumlah balita : ");
//		int balita = input.nextInt();
//		
//		System.out.println("Masukan Jumlah Anak : ");
//		double anak = input.nextInt();
//		
//		//Menghitung porsi Laki-laki dewasa
//		int totalLakiDewasa = 0;
//		totalLakiDewasa = lakiDewasa * 2;
//		
//		//Menghitung porsi Perempuan dewasa
//		int totalPerempuanDewasa = 0;
//		totalPerempuanDewasa = perempuanDewasa * 1;
//		
//		//Menghitung porsi balita
//		int totalBalita = 0;
//		totalBalita = balita *1;
//		
//		//Menghitung porsi anak-anak
//		double totalAnak = 0;
//		totalAnak = anak * 0.5;
//		
//		// Constrain
//		
//		
//		//Menghitung total porsi
//		double hasil = 0;
//		hasil = totalLakiDewasa + totalPerempuanDewasa + totalBalita + totalAnak;
//		
//		int totalHasil = (int)Math.ceil(hasil);
//		
//		//Pengeluaran Output
//		System.out.println(totalHasil + " Porsi");

//		NO 06

//		System.out.println("masukkan password anda :");
//
//		int[] pin = { 1, 2, 3, 4 };
//		String hasilPassword = "";
//		int[] tempPassword = new int[4];
//		int saldoAkhir = 0;
//		for (int i = 0; i < tempPassword.length; i++) {
//			int password = input.nextInt();
//			tempPassword[i] = password;
//			{
//				{
//					if (tempPassword[i] == pin[i]) {
//						hasilPassword = "password anda benar";
//					} else {
//						hasilPassword = "password anda salah";
//					}
//
//				}
//			}
//		}
//		System.out.println(hasilPassword);
//
//		if (hasilPassword.equals("password anda benar")) {
//
//			System.out.println("masukkan jumlah saldo anda :");
//			int saldoAwal = input.nextInt();
//
//			if (saldoAwal == 0) {
//				System.out.println("Silahkan isi saldo terlebih dahulu ");
//				System.out.println("Masukkan jumlah saldo yang akan di isi : ");
//				int saldoIsi = input.nextInt();
//				saldoAwal = saldoAwal + saldoIsi;
//			}
//			input.nextLine();
//			System.out.println("masukkan pilihan transfer anda : ");
//			System.out.println("---Pilih 1 untuk sesama bank---");
//			System.out.println("---Pilih 2 untuk bank lain--- ");
//
//			int pilihanTransfer = input.nextInt();
//			input.nextLine();
//			switch (pilihanTransfer) {
//			case 1:
//				System.out.println("masukkan nomer rekening tujuan : ");
//				String noTujuan = input.nextLine();
//				if (noTujuan.length() == 10) {
//					System.out.println("masukkan nominal transfer : ");
//					int nominalTransfer = input.nextInt();
//
//					if (nominalTransfer >= saldoAwal) {
//						System.out.println("maaf saldo anda kurang !");
//					} else if (nominalTransfer < saldoAwal) {
//						saldoAkhir = saldoAkhir + (saldoAwal - nominalTransfer);
//						System.out.println("saldo anda tersisa sebesar " + saldoAkhir);
//					}
//				} else {
//					System.out.println("silahkan masukkan nomer rekening tujuan yang benar!");
//				}
//				break;
//			case 2:
//				System.out.println("masukkan nomer rekening tujuan (admin Rp. 7500)");
//				String noTujuanLain = input.nextLine();
//				if (noTujuanLain.length() == 10) {
//					System.out.println("masukkan nominal transfer : ");
//					int nominalTransfer = input.nextInt();
//					if (nominalTransfer > saldoAwal) {
//						System.out.println("maaf saldo anda kurang !");
//					} else if (nominalTransfer < saldoAwal) {
//						saldoAkhir = saldoAkhir + (saldoAwal - nominalTransfer - 7500);
//						System.out.println("saldo anda tersisa sebesar " + saldoAkhir);
//					}
//				} else {
//					System.out.println("silahkan masukkan nomer rekening tujuan yang benar! ");
//				}
//				break;
//			default:
//				System.out.println("anda memasukkan pilihan transfer yang salah !");
//			}
//		}

//		NO 07
		
//		System.out.print("Masukan jumlah kartu : ");
//		int kartu = input.nextInt();
//		String yesNo = "" ;
// 
// 
//		do {
//			System.out.print("Jumlah tawaran : ");
//			int tawaran = input.nextInt();
//			System.out.print("Pilih kotak, A atau B : ");
//			String pilih = input.next();
// 
//			Random
//			angka = new Random();
//			int hasilA = 0;
//			int hasilB = 0;
// 
//			for (int i = 0; i < 10; i++) {
//				hasilA = angka.nextInt(10);
//			}
//			for (int i = 0; i < 10; i++) {
//				hasilB = angka.nextInt(10);
//			}
// 
//			if (pilih.equals("A") && hasilA > hasilB) {
//				kartu = kartu + tawaran;
//				System.out.println("Kotak A = " + hasilA);
//				System.out.println("Kotak B = " + hasilB);
//				System.out.println("You Win");
//				System.out.println("kartu yang anda punya = " + kartu);
//			} else if (pilih.equals("A") && hasilA < hasilB) {
//				kartu = kartu - tawaran;
//				System.out.println("Kotak A = " + hasilA);
//				System.out.println("Kotak B = " + hasilB);
//				System.out.println("You Lose");
//				System.out.println("kartu yang anda punya = " + kartu);
//			} else if (pilih.equals("B") && hasilA < hasilB) {
//				kartu = kartu + tawaran;
//				System.out.println("Kotak A = " + hasilA);
//				System.out.println("Kotak B = " + hasilB);
//				System.out.println("You Win");
//				System.out.println("kartu yang anda punya = " + kartu);
//			} else if (pilih.equals("B") && hasilA > hasilB) {
//				kartu = kartu - tawaran;
//				System.out.println("Kotak A = " + hasilA);
//				System.out.println("Kotak B = " + hasilB);
//				System.out.println("You Lose");
//				System.out.println("kartu yang anda punya = " + kartu);
//			}
//			System.out.println("Menyerah ? Yes / No");
//			yesNo= input.next();
// 
//		}while(kartu > 0 && yesNo.equals("no"));{
// 
//		}
		
//		NO 08
//		System.out.print("Input panjang deret : ");
//		int deret = input.nextInt();
// 
//		int genap = 0;
//		int ganjil = 1;
// 
//		int [] deretGenap = new int[deret];
//		int [] deretGanjil = new int[deret];
//		int [] hasil = new int[deret];
// 
//		for (int i = 0; i < deretGenap.length; i++) {
//			deretGenap[i] += genap; 
//			System.out.print(genap + " ");
//			genap += 2;
// 
// 
//		}
//		System.out.println();
// 
//		for (int i = 0; i < deretGanjil.length; i++) {
//			deretGanjil[i] += ganjil; 
//			System.out.print(ganjil + " ");
//			ganjil += 2;
// 
//		}
//		System.out.println();
// 
//		for (int i = 0; i < hasil.length; i++) {
//			hasil[i] =  deretGenap[i] + deretGanjil[i];
//			System.out.print(hasil[i] + " ");
//		}
		
		
//		NO 09
		
//		System.out.print("Masukan Sinyal : ");
//		String sinyal = input.nextLine().toUpperCase();
// 
//		String[] tempSplit = sinyal.split("");
//		String result = "";
// 
//		int gunung = 0;
//		int lembah = 0;
// 
//		for (int i = 0; i < tempSplit.length; i += 2) {
//			result = tempSplit[i] + tempSplit[i + 1];
//			if (result.equals("NT")) {
//				gunung += 1;
//			} else if (result.equals("TN")) {
//				lembah += 1;
//			}
//		}
//		System.out.println("Total gunung yang di lewati : " + gunung);
//		System.out.println("Total lembah yang di lewati : " + lembah);

		
//		NO 10
//		
//		System.out.print("Masukan saldo OPO = Rp. ");
//		int saldo = input.nextInt();
//
//		int hargaNormal = 0;
//		int hargaDiskon = 0;
//		int cashBack = 0;
//		int cup = 0;
//		int saldoDiskon = 0;
//
//		if (saldo >= 40000 && saldo <= 200000) {
//			cup = saldo / 9000;
//			hargaDiskon = cup * 18000 / 2;
//			if (hargaDiskon > 200000) {
//				hargaDiskon = 100000;
//				saldo = (saldo - hargaDiskon);
//				cashBack = hargaDiskon * 10 / 100;
//				if (cashBack > 30000) {
//					cashBack = 30000;
//					saldo = saldo + cashBack;
//				}else {
//					saldo = saldo + cashBack;
//				}
//			} else {
//				saldo = (saldo - hargaDiskon) + (hargaDiskon * 10 / 100);
//			}
//		} else if (saldo > 200000) {
//			saldo = saldo - 200000;
//		} 
//		System.out.println(cup);
//		System.out.println(saldo);

	}
}