package day1;

import java.util.Scanner;

public class Soal3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Belanja : ");
		int belanja = input.nextInt();
		System.out.println("Ongkos kirim : ");
		int ongkir = input.nextInt();
		
		int diskonOngkir = 0;
		int ongkos = 0;
		int diskonbelanja = 0;
		
		if (belanja >=100000) {
			diskonOngkir = 10000;
			diskonbelanja = 20000;
			
		} else if (belanja >=50000) {
			diskonOngkir = 10000;
			diskonbelanja = 10000;
			
		} else if (belanja >=30000) {
			diskonOngkir = 5000;
			diskonbelanja = 5000;
			
			
		}
		
		System.out.println("-----------------------------");
		System.out.println("Belanja : " + belanja);
		System.out.println("Ongkos Kirim : " + ongkir);
		System.out.println("Diskon Ongkir : " + diskonOngkir);
		System.out.println("Diskon Belanja : " + diskonbelanja);
		System.out.println("Total Belanja : " + (belanja + ongkir - diskonOngkir - diskonbelanja));
		
	}

}
