package day3;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Case01 {

	public static void Resolve1() {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

//		No 01
//		System.out.println("Waktu masuk : ");
//		String masuk = input.nextLine();
//
//		System.out.println("Waktu keluar : ");
//		String keluar = input.nextLine();
//
//		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//
//		Date jamMasuk = null;
//		Date jamKeluar = null;
//
//		try {
//			jamMasuk = dateFormat.parse(masuk);
//			jamKeluar = dateFormat.parse(keluar);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//
//		double sum = jamKeluar.getTime() - jamMasuk.getTime();
//		double sumHours = sum / (60 * 60 * 1000);
//		int sumParse =Integer.parseInt(String.valueOf(sumHours));
//		int sumPars =(int)Math.ceil(sumHours);
//		int sumPayment =sumParse*3000;
//		System.out.println(sumParse);
//		System.out.println(sumPayment);
//		input.close();

//		No 02
//		System.out.println("Masukkan tanggal pinjam :");
//		String pinjam = input.next();
//
//		System.out.println("Lama Peminjaman :");
//		int lamaPinjam = input.nextInt();
//		input.nextLine();
//		System.out.println("Masukkan lama Pengembalian :");
//		String kembali = input.next();
//
//		DateFormat formatAwal = new SimpleDateFormat("dd/MM/yyyy");
//
//		Date lamaPeminjaman = null;
//		Date lamaPengembalian = null;
//
//		try {
//			lamaPeminjaman = formatAwal.parse(pinjam); // string diurai menjadi format date
//			lamaPengembalian = formatAwal.parse(kembali);
//			long selisihWaktu =lamaPengembalian.getTime()-lamaPeminjaman.getTime();
//			long selisihHari = selisihWaktu/(24*60*60*1000);
//			
//			if (selisihHari > lamaPinjam) {
//				System.out.println(selisihHari*500);
//			}else {
//				System.out.println("Tepat Waktu");
//				
//			}
//		} catch (Exception e) {
//		e.printStackTrace();
//		}

//		System.out.println("Tanggal masuk : ");
//		String masuk =input.nextLine();
//		
//		System.out.println("Masukkan lama bootcamp : ");
//		String lama = input.next();
//		input.nextLine();
//		
//		System.out.println("Tanggal libur : ");
//		String[]libur=input.next().split(",");
//		
//		SimpleDateFormat memulai = new SimpleDateFormat("dd.MMMM.yyyy");
//		
//		Date mulai=null;
//		Date selesai=null;
//		
//		String nextDate="";
//		
//		try {
//			mulai=memulai.parse(masuk);
//			selesai=memulai.parse(masuk);
//			Calendar startdate = Calendar.getInstance();
//			startdate.setTime(memulai.parse(masuk));
//			System.out.println(startdate);
//			int hitung=0;
//			
//			int day = startdate.get(Calendar.DAY_OF_WEEK);
//			if((day != Calendar.SATURDAY) && (day != Calendar.SUNDAY)) {
//			selesai =memulai.parse(masuk);	
//			}
//			System.out.println(hitung);
//			startdate.add(Calendar.DATE, 10);
//			
//			startdate.add(Calendar.DATE, 10);
//			nextDate = memulai.format(startdate.getTime());
//			System.out.println(nextDate);
//		
//		}

//		No 03

		System.out.println("Masukkan Tanggal Masuk : ");
		String inputTanggalMulai = input.nextLine();

		System.out.println("Masukkan Lama Bootcamp");
		int lama = input.nextInt();
		input.nextLine();

		System.out.println("Masukkan Tanggal Libur");
		String[] libur = input.nextLine().split(",");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

		String nextDate = "";

		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dateFormat.parse(inputTanggalMulai));

			int hitung = 0;
			int tempHitungLibur = 0;
			int i = 0;

			while (i < lama + tempHitungLibur) {
				calendar.add(Calendar.DATE, 1);
				int day = calendar.getTime().getDay();
				int dateofHoliday = calendar.getTime().getDate();

				int j = 0;
				if ((day == 6) || (day == 0)) {
					tempHitungLibur++;
				}
				while (j < libur.length) {
					if (dateofHoliday == Integer.parseInt(libur[j])) {
						tempHitungLibur++;
					}
					j++;
				}
				i++;
			}

			System.out.println("Total Libur = " + (tempHitungLibur + libur.length));
			System.out.println(hitung);
			nextDate = dateFormat.format(calendar.getTime());
			System.out.println(nextDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

//		No 04

//	System.out.println("Masukkan Kata : ");
//	String kataAwal = input.next();
//	
//	int panjang =kataAwal.length();
//	
//	char[]tempArr =kataAwal.toCharArray();
//	String kataBalik="";
//	
//	for (int i = panjang - 1; i>=0; i--) {
//		kataBalik =kataBalik + tempArr[i];
//	}
//	
//	if(kataAwal.equals(kataBalik)) {
//		System.out.println("YES");
//	}else {
//		System.out.println("NO");
//	}
//	System.out.println(kataBalik);
	}
}
