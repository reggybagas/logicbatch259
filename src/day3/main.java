package day3;

import java.util.*;
public class main {
	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println(("Enter the number of case"));
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					Case01.Resolve1();
					break;
				
				default:
					System.out.println("Case is not available");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue ?");
			answer = input.nextLine();

		}
	}
}