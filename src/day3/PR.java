package day3;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class PR {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

//		SOAL WARMUP

//		No 01

//		System.out.println("Masukkan angka1 : ");
//		int a =input.nextInt();
//		System.out.println("Masukkan angka2 : ");
//		int b =input.nextInt();
//		int hasil;
//		hasil = a + b;
//		System.out.println("hasil :" + hasil);

//        No 02

//        System.out.print("Masukkan Waktu : ");
//        String x = input.nextLine().toLowerCase();
//        
//        DateFormat formatAwal = new SimpleDateFormat("hh:mm:ssaa");
//        DateFormat formatAkhir = new SimpleDateFormat("HH:mm:ss");
//       
//        try {
//        	Date date = formatAwal.parse(x);
//        	String output = formatAkhir.format(date);
//        	System.out.println(output);
//        }catch(ParseException y) {
//        	y.printStackTrace();
//        }

//        No 03

//		System.out.println("Masukkan angka : ");
//		int length = input.nextInt();
//		int sum = 0;
//
//		for (int i = 0; i < length; i++) {
//			sum += input.nextInt();
//		}
//
//		System.out.println("Jumlah : " +sum);

//        No 04

//		System.out.println("Masukkan angka : ");
//        int n = input.nextInt();
//        
//        int leftDiagonal = input.nextInt(); //Handles first edge case of the element at matrix pos 0
//        int rightDiagonal = 0;
//        
//        for(int i = 1; i<n*n; i++) //Uses modulus to only sum diagonal elements
//        {
//            int value = input.nextInt();
//            if(i%(n+1) == 0)
//            {
//
//                leftDiagonal += value;
//            }
//            if(i%(n-1) == 0 && i != (n*n)-1)//Handles the second edge case of the last element in the matrix
//            {
//                rightDiagonal += value;
//            }
//        }
//        System.out.println(Math.abs(leftDiagonal-rightDiagonal));

//        No 05

//		System.out.println("Masukkan Panjang nilai : ");
//		int n = input.nextInt();
//
//		double positive = 0;
//		double negative = 0;
//		double yero = 0;
//
//		for (int i = 0; i < n; i++) {
//			int value = input.nextInt();
//			positive += value > 0 ? 1 : 0;
//			negative += value < 0 ? 1 : 0;
//			yero += value == 0 ? 1 : 0;
//		}
//		System.out.println(positive / n);
//		System.out.println(negative / n);
//		System.out.println(yero / n);

//		No 06

//		System.out.println("Masukkan panjang nilai : ");
//		int num = Integer.parseInt(input.nextLine());
//		for (int j = 0; j < num; j++) {
//			for (int i = 1; i <= num; i++) {
//				System.out.print(i < num - j ? " " : "#");
//			}
//			System.out.println("");
//		}

//		No 07

//		System.out.println("Masukkan angka-angka : ");
//		long arr[] = new long[5];
//		for (int arr_i = 0; arr_i < 5; arr_i++) {
//			arr[arr_i] = input.nextLong();
//		}
//		long minVal = 0, maxVal = 0;
//		for (int i = 0; i < 5; i++) {
//			long minf = sumOfNumbers(i, arr);
//			long maxf = sumOfNumbers(i, arr);
//			if (i == 0) {
//				minVal = minf;
//				maxVal = maxf;
//			}
//			if (minf < minVal) {
//				minVal = minf;
//			}
//			if (maxf > maxVal) {
//				maxVal = maxf;
//			}
//		}
//		System.out.println(minVal + " " + maxVal);
//	}
//
//	static long sumOfNumbers(int a, long[] arr) {
//		long sum = 0;
//		for (int i = 0; i < 5; i++) {
//			if (a != i) {
//				sum += arr[i];
//			}
//		}
//		return sum;

//		No 08

//		System.out.println("Masukkan panjang nilai : ");
//		int n = input.nextInt();
//        int tallest = 0;
//        int frequency = 0;
//        
//        
//        for(int i=0; i < n; i++){
//            int height = input.nextInt();
//            
//            if(height > tallest){
//                tallest = height;
//                frequency = 1;
//            }
//            else if(height == tallest) frequency++;
//        }
//        System.out.println(frequency);

//		No 09

//		System.out.println("Masukkan Panjang Nilai : ");
//		int n = input.nextInt();
//        int arr[] = new int[n];
//        long total = 0;
//        for(int i=0; i < n; i++){
//            arr[i] = input.nextInt();
//            total += arr[i];
//        }
//          System.out.println(total);

//		N0 10

//		String angka1 = input.nextLine();
//		String angka2 = input.nextLine();
//
//		String[] arrSplit1 = angka1.split(" "); //string menjadi array (spasi tdk terhitung/dilewatkan)
//		String[] arrSplit2 = angka2.split(" ");
//
//		int[] alice = new int[arrSplit1.length];
//		int[] bob = new int[arrSplit2.length];
//
//		int aliceTotal = 0;
//		int bobTotal = 0;
//
//		for (int i = 0; i < arrSplit2.length; i++) {
//			alice[i] = Integer.parseInt(arrSplit1[i]); //untuk mengconvert string ke integer
//			bob[i] = Integer.parseInt(arrSplit2[i]); 
//
//			if (alice[i] > bob[i]) {
//				aliceTotal += 1;
//			} else if (alice[i] < bob[i]) {
//				bobTotal += 1;
//			}
//		}
//		System.out.println(aliceTotal + " " + bobTotal);

//		SOAL STRINGS

//		No 01

//		System.out.println("Masukkan Kalimat : ");
//		String s = input.nextLine();
//
//		char[] huruf = s.toCharArray();
//		int jumlah = 0;
//		for (int i = 0; i < s.length(); i++) {
//			if (Character.isUpperCase(huruf[i])) { // jika ada karakter uppercase pada huruf[i]
//				jumlah++;
//
//			}
//		}
//		System.out.println("Banyak kata : " + (jumlah + 1));

//		No 02 

//		System.out.println("Panjang password : ");
//		int x = input.nextInt();
//		String password = input.next();
//
//		char[] huruf = password.toCharArray();
//
//		int number = 0;
//		int lower = 0;
//		int upper = 0;
//		int special = 0;
//		int count = 0;
//
//		for (int i = 0; i < password.length(); i++) {
//			if (huruf[i] >= 48 && huruf[i] <= 57) {
//				number++;
//			} else if (huruf[i] >= 65 && huruf[i] <= 90) {
//				upper++;
//			} else if (huruf[i] >= 97 && huruf[i] <= 122) {
//				lower++;
//			} else {
//				special++;
//
//			}
//		}
//		if (number == 0) {
//			count++;
//		}
//		if (upper == 0) {
//			count++;
//		}
//		if (lower == 0) {
//			count++;
//		}
//		if (special == 0) {
//			count++;
//		}
//		if(password.length() >=6) {
//			System.out.print(count);
//		}else {
//			System.out.println(count + (6-(password.length()+count)));
//		}

//		No 03

//		int n = input.nextInt();
//		input.nextLine();
//		String s = input.nextLine();
//		int k = input.nextInt() % 26;
//		StringBuilder output = new StringBuilder("");
//
//		for (char letter : s.toCharArray()) {
//			if (letter > 64 && letter < 91)// Capital letter
//			{
//				char encrypted = (char) (letter + k);
//				if (encrypted > 90) {
//					encrypted = (char) ((encrypted % 90) + 64);
//				}
//				output.append(encrypted);
//			} else if (letter > 96 && letter < 123)// Lower case letter
//			{
//				char encrypted = (char) (letter + k);
//				if (encrypted > 122) {
//					encrypted = (char) ((encrypted % 122) + 96);
//				}
//				output.append(encrypted);
//			} else// Symbol
//			{
//				output.append(letter);
//			}
//		}
//		System.out.println(output);

//		No 04

//		String S = input.next();
//		int count = 0;
//		int currentPos = 0;
//		for (char letter : S.toCharArray()) {
//
//			if (currentPos % 3 == 1) {
//				count += (letter != 'O') ? 1 : 0;
//			} else {
//				count += (letter != 'S') ? 1 : 0;
//			}
//			currentPos++;
//		}
//		System.out.println(count);

//        No 05

//		int q = input.nextInt();
//		queries: for (int a0 = 0; a0 < q; a0++) {
//			String s = input.next();
//			char[] find = "hackerrank".toCharArray();
//			int findIndex = 0;
//
//			for (char c : s.toCharArray()) {
//				if (find[findIndex] == c)
//					findIndex++;
//
//				if (findIndex == find.length) { // We ran out of characters to find
//					System.out.println("YES");
//					continue queries;
//				}
//
//			}
//			System.out.println("NO"); // We didn't find all characters
//		}

//		No 06

//		System.out.print("Masukkan Kalimat : ");
//		String kalimat = input.nextLine().replaceAll(" ", "").toLowerCase();
//
//		String hasil = "";
//
//		for (int i = 'a'; i < 'y'; i++) {
//			if (kalimat.indexOf(i) != -1) {
//				hasil += i;
//
//			}
//		}
//		if (hasil.length() == 26) {
//			System.out.println("Pangram");
//
//		} else {
//			System.out.println("Bukan Pangram");
//		}
		
//		No 08
		
//		int numberM= input.nextInt();
//		
//		String inputChar ="";
//		int[]count= new int[26];
//		int[]temp;
//		
//		for (int i = 0; i < numberM; i++) {
//			temp= new int[26];
//			inputChar=input.next();
//			for (int c : inputChar.toCharArray()) {
//				temp [c-97]++;
//				if (temp[c-97]==1)
//					count[c-97]++;
//			}
//			temp=null;
//		}
//		int sum=0;
//		for (int i = 0; i < 26; i++) {
//			if(count[i]==numberM)
//				sum+=1;
//		}
//		count=null;
//		System.out.println(sum);
		
//		No 09
		
//		String kata1 = input.next();
//		String kata2 = input.next();
//		
//		char[]arr1 = new char[kata1.length()];
//		char[]arr2= new char [kata2.length()];
//		
//		for (int i = 0; i < kata1.length(); i++) {
//			arr1[i]=kata1.charAt(i);
//		}
//		for (int i = 0; i < kata2.length(); i++) {
//			arr2[i]=kata2.charAt(i);
//		}
//		int count =0;
//		for (int i = 0; i < kata1.length(); i++) {
//			for (int j = 0; j < kata2.length(); j++) {
//				if(arr1[i] == arr2[j]) {
//					count++;
//					arr2[j]='0';
//					break;
//			}
//			
//		}
//			
//		}
//			
//		System.out.println((kata1.length()+kata2.length()) - (2*count));
		

//			No 10

//		Set<Character> s1Set = new HashSet<>();
//		Set<Character> s2Set = new HashSet<>();
//		System.out.println("Masukkan kata pertama : ");
//		String s1 = input.nextLine();
//		for (char c : s1.toCharArray()) {
//			s1Set.add(c);
//		}
//		System.out.println("Masukkan kata kedua : ");
//		String s2 = input.nextLine();
//		for (char c : s2.toCharArray()) {
//			s2Set.add(c);
//		}
//		s1Set.retainAll(s2Set);
//		if (!s1Set.isEmpty()) {
//			System.out.println("yes!");
//		} else {
//			System.out.println("no!");
//		}
//		System.out.println("Masukkan kata pertama : ");
//		String kataPertama = input.next();
//		System.out.println("Masukkan kata kedua : ");
//		String kataKedua = input.next();
//		String hasil = "";
		
//		PR MEAN MEDIAN MODE
		
		DecimalFormat y = new DecimalFormat("#.##");

		System.out.println("Banyak data : ");
		int n = input.nextInt();

		double[] nilai = new double[n];
		double mean = 0;
		double median = 0;
		double modus = 0;
		double jumlah = 0;

		System.out.println("Masukkan data : ");
		for (int i = 0; i < n; i++) {
			int x = input.nextInt();
			nilai[i] = x;
		}

		Arrays.sort(nilai);
		System.out.println(Arrays.toString(nilai));

		// menghitung mean
		for (int i = 0; i < n; i++) {
			jumlah = jumlah + nilai[i];
		}

		mean = jumlah / n; //angka input / n(banyak array)

		// menentukan median
		if (n % 2 == 1) { //5 mod 2 = 1 (n = ganjil)
			median = nilai[((n - 1) / 2)]; 
		} else if (n % 2 == 0) {
			median = ((nilai[(n / 2) - 1] + nilai[n / 2]) / 2);
		}

		// menentukan modus

		int max = 0;
		for (int i = 0; i < n; i++) {
			int count = 0;
			for (int j = 0; j < n; j++) {
				if (nilai[j] == nilai[i]) {
					count++;
				}
				if (count > max) {
					max = count;
					modus = nilai[i];
				}
			}
		}

		System.out.println("Mean   = " + y.format(mean));
		System.out.println("Median = " + y.format(median));
		System.out.println("Modus  = " + modus);
		
		
	}
}
