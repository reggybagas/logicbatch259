package day3;

import java.text.*;
import java.util.*;

public class LatihanInputString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

//	no 01
//		
//		System.out.print("Masukkan Uang Andi : ");
//		int uangAndi = input.nextInt();
//
//		System.out.print("Masukkan Daftar Baju : ");
//		int[] baju = new int[4];
//		for (int i = 0; i < baju.length; i++) {
//			int daftar = input.nextInt();
//			baju[i] = daftar;
//		}
//		System.out.print("Masukkan Daftar Celana : ");
//		int[] celana = new int[4];
//		for (int j = 0; j < celana.length; j++) {
//			int daftar = input.nextInt();
//			celana[j] = daftar;
//		}
//		input.close();
//
//		int total =0;
//		int maksimal = 0;
//		
//		for (int i = 0; i < celana.length; i++) {
//			total = baju[i] + celana[i];
//			if(total <= uangAndi && total > maksimal) {
//				maksimal = total;
//				
//			}
//		}
//		System.out.print(maksimal + " ");

//		no 02
//		System.out.println("Masukkan Banyak Angka :");
//		int n = input.nextInt();
//		
//		int[]arr= new int [n];
//		System.out.println("arr =");
//		for (int i = 0; i < n; i++) {
//			int angka = input.nextInt();
//			arr[i] = angka;
//		}
//
//		System.out.print("Rotasi =");
//		int rotasi = input.nextInt();
//
//		int[] x = new int[rotasi];
//		for (int j = 0; j < rotasi; j++) {
//			x[j] = arr[0];
//			for (int i = 0; i < arr.length - 1; i++) {
//				arr[i] = arr[i + 1];
//				System.out.print(arr[i]);
//			}
//			arr[arr.length - 1] = x[j];
//			System.out.println(x[j]);
//		}

//		no 03
//		System.out.println("Input putung rokok : ");
//		int n = input.nextInt();
//		
//		int batangRokok = n / 8;
//		int penghasilan = batangRokok* 500;
//		int sisa = n % 8;
//		
//		System.out.println("Jumlah batang rokok :"+ batangRokok);
//		System.out.println("Sisa batang rokok :" + sisa);
//		System.out.println("Penghasilan : "+ penghasilan);

//		no 04

//		System.out.println("Total Menu : ");
//		int n = input.nextInt();
//		
//		System.out.println("Makan alergi index ke- : ");
//		int alergi = input.nextInt();
//		
//		System.out.println("Harga Menu :");
//		int[] hargaMenu = new int[n];
//		
//		for (int i = 0; i < hargaMenu.length; i++) {
//			int menu = input.nextInt();
//			hargaMenu[i] = menu;
//		}
//		
//		System.out.println("Uang Elsa : ");
//		int uangElsa = input.nextInt();
//		
//		int total =0;
//		for (int i = 0; i < hargaMenu.length; i++) {
//			total = total + hargaMenu[i];
//		}
//		int jumlah = total - hargaMenu[alergi];
//		int makanElsa = jumlah / 2;
//		int sisaUang = uangElsa - makanElsa;
//		
//		System.out.println("Total makanan yang dimakan Elsa dan Dimas : " + total);
//		System.out.println("Makanan yang dimakan Elsa :" + jumlah);
//		
//		System.out.println();
//		
//		System.out.println("Elsa harus membayar : " + makanElsa);
//		
//		if(sisaUang == 0) {
//			System.out.println("Uang pas : "+ sisaUang);
//		}else if (sisaUang < makanElsa) {
//			System.out.println("Elsa kurang membayar : " + (sisaUang*(-1)));
//		}
//		else {
//			System.out.println("Sisa Uang Elsa : " + sisaUang);
//		}

//		no 05

//		System.out.println("Silahkan ketik : ");
//		replaceAll untuk mengganti suatu character menjadi apa yang kita inginkan
//		toLowerCase huruf besar menjadi huruf kecil
//		String x =input.nextLine().toLowerCase().replace(" ", "");
//		
//		char[]tempArr =x.toCharArray();
//		
//		String resultVokal = "";
//		String resultKonsonan ="";
//		
//		
//		Arrays.sort(tempArr);
//		for (int i = 0; i < tempArr.length; i++) {
//			if(tempArr[i] == 'a' || tempArr[i] == 'i' || tempArr[i] == 'u'
//					|| tempArr[i]=='e' || tempArr[i] == 'o') {
//				resultVokal += tempArr[i];
//			}else {
//				resultKonsonan += tempArr[i];
//			}
//		}
//		System.out.println("Vokal : " + resultVokal);
//		System.out.println("Konsonan : " + resultKonsonan);
//		System.out.println(tempArr);
//		
//		no 06

//		System.out.print("Masukkan Kalimat :");
//		String kata = input.nextLine(); 
//		String[]tempSplit = kata.split(" "); //mecah kata menjadi indeks ke 0
//		String result= "";
//		
//		for (int i = 0; i < tempSplit.length; i++){
//			char[]tempArr = tempSplit[i].toCharArray(); //mecah per kata jadi s a y a
//			for (int j = 0; j < tempArr.length; j++) {
//				if(j % 2 == 1) {
//					result += "*";
//			}else {
//				result += tempArr[j];
//			}
//		}
//			result+=" ";
//		}
//			System.out.print(result);
//			

//		PR HARI RABU DAY 3
//		no 01 
//		
//		System.out.print("Masukkan Input : ");
//		int n = input.nextInt();
//		
//		int fibonacci[] = new int[n]; // n = panjang dari arraynya
//		
//		fib[0]=1; // indeks ke 0
//		fib[1]=1;
//		
//		for (int i = 2; i < n; i++) { // 2 = dimulai indeks ke 2
//			fibonacci[i]= fibonacci[i-1]+ fibonacci[i-2];
//		}
//		for (int i = 0; i < n; i++) {
//			System.out.print(fibonacci[i] + " ");
//		} 
//				

//		NO 02

		System.out.print("Masukkan Input : ");
		int n = input.nextInt();
		
		int fibonacci[] = new int[n];
		
		fibonacci[0]=1;
		fibonacci[1]=1;
		fibonacci[2]=1;
		
		for (int i = 3; i < n; i++) {
			fibonacci[i]= fibonacci[i-1]+ fibonacci[i-2] + fibonacci[i-3];
		}
		for (int i = 0; i < n; i++) {
			System.out.print(fibonacci[i] + " ");
			
		}

//		no 03

//		System.out.print("Input Bilangan : ");
//		int n = input.nextInt();
//		int bilangan;
//
//		for (int i = 2; i <= n; i++) {
//			bilangan = 0;
//			for (int j = 1; j <= i; j++) {
//				if (i % j == 0) {
//					bilangan = bilangan + 1;
//				}
//			}
//			if (bilangan == 2) {
//				System.out.print(i + " ");
//			}
//		}

//		no 04

//		System.out.print("Masukkan Waktu : ");
//        String x = input.nextLine().toLowerCase();
//        
//        DateFormat formatAwal = new SimpleDateFormat("hh:mm:ssaa");
//        DateFormat formatAkhir = new SimpleDateFormat("HH:mm:ss");
//       
//        try {
//        	Date date = formatAwal.parse(x);
//        	String output = formatAkhir.format(date);
//        	System.out.println(output);
//        }catch(ParseException z) {
//        	z.printStackTrace();
//        }

//        no 05

//		System.out.print("Menentukan pohon faktor dari : ");
//		int n = input.nextInt();
//		int x = 0;
//		String result = "";
//
//		for (int i = 2; i <= n; i++) {
//			for (int j = 1; j <= i; j++) {
//				if (n % i == 0) {
//					x = n / i;
//					System.out.println(n + "/" + i + "=" + x);
//					n = x;
//				}
//			}
//		}

//				
//			}
//		}

//		no 01 Hari Jumat DAY 4

//		System.out.println("Masukkan Jarak: ");
//		int tempJarak[]= new int[4]; //membuat variabel int array ,4 input yg kita inginkan, 
//		double total =0;
//		double bensin =0;
//		for (int i = 0; i < 4; i++) { //4 input yg kita inginkan
//			int jarak = input.nextInt();
//			tempJarak[i] = jarak ;
//		}
//		input.nextLine();
//		System.out.println("Masukkan Indeks Jarak :");
//		String[] inputIndeksSplit = input.nextLine().split(" "); //membuat variabel split String indeks ke 0 1 2 3 menjadi 0123
//		
//		for (int i = 0; i < inputIndeksSplit.length; i++) {
//			total += tempJarak[Integer.parseInt(inputIndeksSplit[i])-1]; //parseint untuk mencetak integer array . -1 untuk memulai dari indeks ke 0(1-1)
//		}
//		System.out.println(total);
//		bensin = total / 2500;
//		System.out.println(Math.ceil(bensin)); //mathceil membulatkan nilai keatas
//		

//		no 02

//		System.out.println("Input Sinyal SOS : ");
//		String sinyal = input.next().toUpperCase();
		// toUpperCase digunakan untuk merubah nilai string ke nilai string yang terdiri
		// dari huruf besar semua
		// toLowerCase digunakan untuk merubah nilai string ke nilai string yang terdiri
		// dari huruf kecil semua
//		char[] huruf = sinyal.toCharArray();
		// toCharArray mengubah string yang ditentukan menjadi karakter baru

//		int count = 0;
//		for (int i = 0; i < sinyal.length(); i += 3) {
//			if (huruf[i] != 'S' || huruf[i + 1] != 'O' || huruf[i + 2] != 'S') {
//				count++;
//
//			}
//		}
//		System.out.println(count);

	}

}