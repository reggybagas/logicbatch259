package testFT1;

import java.util.Scanner;

public class Soal02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int arrJarak[] = {500,2000,3500,5000};
		int total = 0;
		double waktu = 0;

		System.out.println("jarak :");
		for (int i = 0; i < arrJarak.length; i++) {
			int jarak = input.nextInt();
			arrJarak[i] = jarak;
		}
		input.nextLine();

		System.out.println("masukkan index jarak ");
		String inputIndexSplit[] = input.nextLine().split(" ");

		for (int i = 1; i < inputIndexSplit.length; i++) {
			total += arrJarak[Integer.parseInt(inputIndexSplit[i]) - 1];
		}
		double totalJarak = total / 1000;
		System.out.println("total jarak : " + totalJarak + "km");
		waktu = ((totalJarak / 30) * 60 + (10 * (inputIndexSplit.length-1)));
		System.out.println(waktu + "menit");
	}
}
