package testFT1;

import java.util.Arrays;
import java.util.Scanner;

public class Soal06 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Silahkan ketik : ");

		String kata = input.nextLine().toLowerCase().replaceAll(" ", "");

		char[] huruf = kata.toCharArray();
		String vokal = "";
		String konsonan = "";

		Arrays.sort(huruf);
		for (int i = 0; i < huruf.length; i++) {
			if (huruf[i] == 'a' || huruf[i] == 'i' || huruf[i] == 'u' || huruf[i] == 'e' || huruf[i] == 'o') {
				vokal += huruf[i];
			} else {
				konsonan += huruf[i];
			}
		}
		System.out.println("Huruf Vokal adalah : " + vokal);
		System.out.println("Huruf Konsonan adalah : " + konsonan);

	}

}
