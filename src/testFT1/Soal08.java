package testFT1;

import java.util.Scanner;

public class Soal08 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Masukkan Kalimat :");
		String kata = input.nextLine();
		String[] tempSplit = kata.split(" ");
		String result = "";

		for (int i = 0; i < tempSplit.length; i++) {
			char[] tempArr = tempSplit[i].toCharArray();
			for (int j = 0; j < 3; j++) {
				if (j == 0) {
					result += tempArr[0];
				} else if (j == 2) {
					result += tempArr[tempArr.length - 1];
				} else {
					result += "***";
				}
			}
		}

		System.out.print(result + " ");

	}
}
