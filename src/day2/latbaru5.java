package day2;

import java.util.Scanner;

public class latbaru5 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan nilai n : ");
		int n = input.nextInt();
		int nilaiawal = 1;

		int[] array = new int[n];
		for (int i = 0; i < array.length; i++) {
			if (i % 3 == 2) {
				System.out.print("*");
			} else {

				array[i] = nilaiawal;
				System.out.print(array[i] + " ");
				nilaiawal += 4;
			}
		}
	}
}
